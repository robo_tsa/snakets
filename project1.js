var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var canvas;
var ctx;
canvas = document.getElementById('canvas');
var ctx = canvas.getContext('2d');
//var w = document.getElementById("canvas").getAttribute("width");
//alert(w);
//var h = document.getElementById("canvas").getAttribute("height");
//alert(h);
var snakeArray;
//var size = 10;
var direction;
var food;
var score;
var Food = (function () {
    function Food(size) {
        this.size = 10;
        this.w = 640;
        this.h = 480;
        this.x = 0;
        this.y = 0;
        this.size = size;
        //this.w=w;
        //this.h=h;
        //this.x=x;
        //this.y=y;
    }
    Food.prototype.createFood = function () {
        food = {
            x: Math.floor(Math.random() * (this.w / this.size)),
            y: Math.floor(Math.random() * (this.h / this.size))
        };
    };
    Food.prototype.paintCell = function (x, y) {
        ctx.fillStyle = "blue";
        ctx.fillRect(this.x * this.size, this.y * this.size, this.size, this.size);
        ctx.strokeStyle = "white";
        ctx.strokeRect(this.x * this.size, this.y * this.size, this.size, this.size);
    };
    return Food;
}());
//var food2 = new Food (10);
var Snake = (function () {
    function Snake(length) {
        this.length = length;
        this.snakeArray = snakeArray;
        this.direction = direction;
        // this.nx=nx;
        //this.ny=ny;
    }
    //funkcja tworzaca weza
    Snake.prototype.createSnake = function () {
        var length = 5;
        snakeArray = [];
        for (var i = length - 1; i > 0; i--) {
            snakeArray.push({ x: i, y: 0 }); //poprawic petle aby szla od pierwszej gornej komorki
        }
    };
    return Snake;
}());
;
var Game = (function (_super) {
    __extends(Game, _super);
    function Game(tail, game_loop) {
        var _this = _super.call(this, length) || this;
        _this.w = 640;
        _this.h = 480;
        _this.tail = tail;
        _this.game_loop = game_loop;
        _this.w = 640;
        _this.h = 480;
        _this.size = 10;
        return _this;
    }
    Game.prototype.paint = function () {
        ctx.fillStyle = "white";
        ctx.fillRect(0, 0, this.w, this.h);
        ctx.strokeStyle = "black";
        ctx.strokeRect(0, 0, this.w, this.h);
        var nx = snakeArray[0].x; //pozycja glowy weza w x
        var ny = snakeArray[0].y; //pozycja glowy weza w y
        //nx++;
        //funkcja sterowania 
        if (direction == "right")
            nx++;
        else if (direction == "left")
            nx--;
        else if (direction == "up")
            ny--;
        else if (direction == "down")
            ny++;
        document.addEventListener("keydown", KeyDown, false);
        function KeyDown(e) {
            if (e.keyCode == "37" && direction != "right")
                direction = "left";
            else if (e.keyCode == "38" && direction != "down")
                direction = "up";
            else if (e.keyCode == "39" && direction != "left")
                direction = "right";
            else if (e.keyCode == "40" && direction != "up")
                direction = "down";
        }
        //funckje kolizji
        function collission(x, y, array) {
            for (var i = 3; i < array.length; i++) {
                if (array[i].x == x && array[i].y == y) {
                    return true;
                }
                return false;
            }
        }
        //wykrywanie kolizji    
        if (nx == -1 || nx == this.w / this.size || ny == -1 || ny == this.h / this.size || collission(nx, ny, snakeArray)) {
            game.init();
            return;
        }
        //zjedzenie owocu    
        if (nx == food.x && ny == food.y) {
            var tail = { x: nx, y: ny };
            score++;
            food2.createFood();
        }
        else {
            var tail_1 = snakeArray.pop();
            tail_1.x = nx;
            tail_1.y = ny;
        }
        snakeArray.unshift(tail);
        //funkcja rysowania komorki
        function paintCell(x, y) {
            ctx.fillStyle = "blue";
            ctx.fillRect(x * this.size, y * this.size, this.size, this.size);
            ctx.strokeStyle = "white";
            ctx.strokeRect(x * this.size, y * this.size, this.size, this.size);
        }
        //implementacja rysowaia weza i jedzenia
        for (var i = 0; i < snakeArray.length; i++) {
            var c = snakeArray[i];
            paintCell(c.x, c.y);
        }
        paintCell(food.x, food.y);
    };
    ;
    Game.prototype.init = function () {
        direction = "right";
        snake.createSnake();
        food2.createFood();
        if (typeof this.game_loop != "undefined") {
            clearInterval(this.game_loop);
        }
        ;
        this.game_loop = setInterval(game.paint(), 100);
    };
    return Game;
}(Food));
var game = new Game(this.tail, this.game_loop);
var food2 = new Food(10);
var snake = new Snake(10);
game.init();
game.paint();
