var canvas: HTMLCanvasElement;
var ctx: CanvasRenderingContext2D;

canvas = <HTMLCanvasElement>document.getElementById('canvas')
var ctx = canvas.getContext('2d');


var snakeArray;

var direction;
var food;
var score:number;




class Food {

public size:number  = 10;
public w:number  = 640;
public h:number = 480;
public x:number = 0;
public y:number = 0;


constructor(size:number){//, w:number, h:number, x:number,y:number){

this.size=size;
//this.w=w;
//this.h=h;
//this.x=x;
//this.y=y;

}

createFood(){
    
    
    food = {
        x:Math.floor(Math.random() * (this.w/this.size)),
        y:Math.floor(Math.random() * (this.h/this.size)),
    
    
};}


paintCell(x,y){
    ctx.fillStyle="blue";
    ctx.fillRect(this.x*this.size,this.y*this.size,this.size,this.size);
    ctx.strokeStyle="white";
    ctx.strokeRect(this.x*this.size,this.y*this.size,this.size,this.size);
    
}



}


class Snake {

public length:number;
public snakeArray: any;
public direction: string;
public nx: number;
public ny: number;




constructor(length:number){//, snakeArray:any, direction:string, nx:number, ny:number){
    this.length=length;
    this.snakeArray=snakeArray;
    this.direction=direction;
   // this.nx=nx;
    //this.ny=ny;

}

//funkcja tworzaca weza
createSnake(){
    
    var length = 5;
    snakeArray=[];
    for(var i:number = length-1; i>0; i--){
        snakeArray.push({x:i, y:0}); //poprawic petle aby szla od pierwszej gornej komorki
        
    }
    
}



 };   
    

class Game extends Food {

public tail:number;
public game_loop:any;
public w:number  = 640;
public h:number = 480;
public size:number;

constructor(tail:number, game_loop:any){
super(length);

this.tail=tail;
this.game_loop=game_loop;
this.w=640;
this.h=480;
this.size=10;
}


paint() {
   
ctx.fillStyle="white";
ctx.fillRect(0,0,this.w,this.h);
ctx.strokeStyle="black";
ctx.strokeRect(0,0,this.w,this.h);

    
  
    var nx=snakeArray[0].x; 
    var ny=snakeArray[0].y; 
  
    
//funkcja sterowania 
    
    if(direction == "right") nx++;
    else if(direction == "left") nx--;
    else if (direction == "up") ny--;
    else if(direction == "down") ny++;    
    
    
   document.addEventListener("keydown", KeyDown, false);
   function KeyDown(e){
           if (e.keyCode == "37" && direction != "right") direction = "left";
           else if(e.keyCode == "38" && direction != "down") direction = "up";
           else if(e.keyCode == "39" && direction != "left") direction = "right";
           else if(e.keyCode == "40" && direction != "up") direction = "down";
  }
  
  //funckje kolizji
    
    function collission(x,y,array){
    for(var i = 3; i<array.length; i++){
        if(array[i].x == x && array[i].y == y){
            return true;
            
        }
        return false;
    }
    
    
}

    
//wykrywanie kolizji    
    
    if(nx == -1 || nx == this.w/this.size || ny== -1 || ny == this.h/this.size || collission(nx,ny,snakeArray))
       
       {
       
       game.init();
       return;
       
       }  
    
    
//zjedzenie owocu    
     if(nx == food.x && ny == food.y){
        
        var tail = {x:nx,y:ny};
        score++;
        food2.createFood();
     
     }
    
    else{
    
    let tail = snakeArray.pop();
    tail.x = nx;
    tail.y = ny;
    
    }
    
    
    
    
    
    snakeArray.unshift(tail);
    
    
    //funkcja rysowania komorki
    function paintCell(x,y,z){
    ctx.fillStyle="blue";
    ctx.fillRect(x*this.size,y*this.size,this.size,this.size);
    ctx.strokeStyle="white";
    ctx.strokeRect(x*this.size,y*this.size,this.size,this.size);
    
}

    //implementacja rysowaia weza i jedzenia
        
    for(var i=0; i<snakeArray.length; i++){
    var c = snakeArray[i]; 
    paintCell(c.x,c.y,"blue");
    
    }
    paintCell(food.x,food.y,"red");
    
    var scoretext = "Wynik: " + score;
    ctx.fillText(scoretext,5,475);
    ctx.font = "25px Arial";
}; 


init(){
    direction = "right";
    snake.createSnake();
    food2.createFood()
    
    
    if(typeof this.game_loop !="undefined") {clearInterval(this.game_loop)};
    this.game_loop = setInterval(game.paint(),100);
    

}

}



var game:Game = new Game(this.tail,this.game_loop);
var food2:Food = new Food(10);
var snake:Snake = new Snake(10);


game.init();
game.paint();



